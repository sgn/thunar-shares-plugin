# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Dušan Kazik <prescott66@gmail.com>, 2020
# Robert Hartl <hartl.robert@gmail.com>, 2009-2010
# 7dcd6f74323fe8d9c477949ff8fcbb1c_c427b63 <3fcd202e3dfab15fda15b8e88e54d449_7173>, 2011
msgid ""
msgstr ""
"Project-Id-Version: Thunar Plugins\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-05 00:46+0100\n"
"PO-Revision-Date: 2013-07-03 18:10+0000\n"
"Last-Translator: Dušan Kazik <prescott66@gmail.com>, 2020\n"
"Language-Team: Slovak (http://www.transifex.com/xfce/thunar-plugins/language/sk/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: sk\n"
"Plural-Forms: nplurals=4; plural=(n % 1 == 0 && n == 1 ? 0 : n % 1 == 0 && n >= 2 && n <= 4 ? 1 : n % 1 != 0 ? 2: 3);\n"

#: ../libshares/libshares-util.c:166
msgid "Please, write a name."
msgstr "Zadajte názov."

#. Warn the user
#: ../libshares/libshares-util.c:174
msgid ""
"Share name too long. Some old clients may have problems to access it, "
"continue anyway?"
msgstr "Názov zdieľanej položky je príliš dlhý. Niektorí starší klienti môžu mať problémy s prístupom. Chcete napriek tomu pokračovať?"

#: ../libshares/libshares-util.c:187
#, c-format
msgid "Error while getting share information: %s"
msgstr "Pri získavaní informácií o zdieľaní došlo k chybe: %s"

#: ../libshares/libshares-util.c:197
msgid "Another share has the same name"
msgstr "Iná zdieľaná položka má rovnaký názov"

#: ../libshares/libshares-util.c:251
msgid "Cannot modify the share:"
msgstr "Nemožno zmeniť zdieľaný priečinok:"

#: ../libshares/libshares-util.c:353
msgid ""
"Thunar needs to add some permissions to your folder in order to share it. Do"
" you agree?"
msgstr "Aplikácia Thunar pridá tomuto priečinku oprávnenia, aby ho mohla zdieľať. Chcete pokračovať?"

#: ../libshares/libshares-util.c:392
msgid "Error when changing folder permissions."
msgstr "Chyba pri nastavovaní práv k priečinku."

#: ../libshares/shares.c:165
#, c-format
msgid "%s %s %s returned with signal %d"
msgstr "%s %s %s sa vrátil so signálom %d"

#: ../libshares/shares.c:174
#, c-format
msgid "%s %s %s failed for an unknown reason"
msgstr "%s %s %s zlyhal z neznámeho dôvodu"

#: ../libshares/shares.c:195
#, c-format
msgid "'net usershare' returned error %d: %s"
msgstr "Príkaz 'net usershare' vrátil chybu %d: %s"

#: ../libshares/shares.c:197
#, c-format
msgid "'net usershare' returned error %d"
msgstr "Príkaz 'net usershare' vrátil chybu %d"

#: ../libshares/shares.c:231
#, c-format
msgid "the output of 'net usershare' is not in valid UTF-8 encoding"
msgstr "výstup príkazu 'net usershare' nie je platný reťazec s kódovaním UTF-8"

#: ../libshares/shares.c:490 ../libshares/shares.c:702
#, c-format
msgid "Failed"
msgstr "Nepodarilo sa"

#: ../libshares/shares.c:592
#, c-format
msgid "Samba's testparm returned with signal %d"
msgstr "Testovací parameter aplikácie Samba vrátil signál %d"

#: ../libshares/shares.c:598
#, c-format
msgid "Samba's testparm failed for an unknown reason"
msgstr "Testovací parameter aplikácie Samba z neznámeho dôvodu zlyhal"

#: ../libshares/shares.c:613
#, c-format
msgid "Samba's testparm returned error %d: %s"
msgstr "Testovací parameter aplikácie Samba vrátil chybu %d: %s"

#: ../libshares/shares.c:615
#, c-format
msgid "Samba's testparm returned error %d"
msgstr "Testovací parameter aplikácie Samba vrátil chybu %d"

#: ../libshares/shares.c:784
#, c-format
msgid "Cannot remove the share for path %s: that path is not shared"
msgstr "Nie je možné odobrať zdieľanie cesty %s: táto cesta nie je zdieľaná"

#: ../libshares/shares.c:837
#, c-format
msgid ""
"Cannot change the path of an existing share; please remove the old share "
"first and add a new one"
msgstr "Nie je možné zmeniť cestu existujúcej zdieľanej zložky; najprv odoberte straú zdieľanú zložku a potom pridajte novú"

#: ../thunar-plugin/tsp-page.c:150
msgid "<b>Folder Sharing</b>"
msgstr "<b>Zdieľanie priečinka</b>"

#. Share check button
#: ../thunar-plugin/tsp-page.c:160
msgid "Share this folder"
msgstr "Zdieľať tento priečinok"

#. Share name
#: ../thunar-plugin/tsp-page.c:171
msgid "Share Name:"
msgstr "Názov zdieľaného priečinka:"

#. Share comments
#: ../thunar-plugin/tsp-page.c:181
msgid "Comments:"
msgstr "Komentáre:"

#. Write access
#: ../thunar-plugin/tsp-page.c:190
msgid "Allow others users to write in this folder"
msgstr "Povoliť ostatným užívateľom zápis do tohto priečinka"

#. Guest access
#: ../thunar-plugin/tsp-page.c:196
msgid "Allow Guest access"
msgstr "Povoliť prístup neprihláseným používateľom"

#. Apply button
#: ../thunar-plugin/tsp-page.c:201
msgid "_Apply"
msgstr "_Použiť"

#: ../thunar-plugin/tsp-page.c:292
msgid "Share"
msgstr "Zdieľaný priečinok"

#: ../thunar-plugin/tsp-page.c:364
msgid "You are not the owner of the folder."
msgstr "Nieste vlastník priečinka."

#: ../thunar-plugin/tsp-page.c:372
msgid ""
"You may need to install Samba, check your user permissions (usershares group) and re-login.\n"
"<b>More info:</b> <u>https://docs.xfce.org/xfce/thunar/thunar-shares-plugin</u>"
msgstr ""
